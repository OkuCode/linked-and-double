"""Localization Class"""


class Location:
    def __init__(self, code: str, description: str):
        self.__code = code
        self.__description = description

    @property
    def code(self):
        return self.__code

    @code.setter
    def code(self, value):
        # Validations
        self.__code = value

    @property
    def description(self):
        return self.__description

    @description.setter
    def description(self, value):
        self.__description = value

    def __repr__(self):
        return f"Location({self.__code}, {self.__description})"
