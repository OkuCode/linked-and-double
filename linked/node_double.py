""" Node for the double linked list """

from linked.child import Child


class Node:
    def __init__(self, data: Child, next: None, prev: None):
        self.data = data
        self.next = next
        self.prev = prev
