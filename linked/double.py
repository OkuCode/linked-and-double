"""Double linked list comes here, all the methods"""

from linked.node_double import Node


class DoubleLinkedList:
    def __init__(self):
        self.head = None

    # Display data
    def display_data(self):
        if self.head is None:
            raise Exception("Invalid Operation, No data available")
        else:
            temp = self.head
            temp_str = ""
            while temp:
                temp_str += " <---- " + str(temp.data) + ' ----> '
                temp = temp.next
            print(temp_str)

    # Insert data to end
    def append_data(self, data):
        if self.head is None:
            node = Node(data, None, None)
            node.prev = None
            self.head = node
        elif self.head.data == data:
            print("Data already on the list")
        else:
            node = Node(data, None, None)
            temp = self.head
            while temp.next:
                temp = temp.next
            temp.next = node
            node.prev = temp
            node.next = None

    # Insert to begin
    def insert_begin(self, data):
        if self.head is None:
            node = Node(data, None, None)
            self.head = node
            node.next = self.head
        elif self.head.data == data:
            raise Exception("Data already on the list")
        else:
            node = Node(data, None, None)
            self.head.prev = node
            node.next = self.head
            self.head = node

    # Insert by index
    def insert_data_by_index(self, data, index):
        if index < 0 or index >= self.count_nodes():
            raise IndexError("Invalid Position")
        elif index == 0:
            self.insert_begin(data)
        else:
            count = 0
            temp = self.head
            while temp:
                if count == index - 1:
                    node = Node(data, None, None)
                    nxt = temp.next
                    temp.next = node
                    node.next = nxt
                    node.prev = temp
                    break
                temp = temp.next
                count += 1

    # Insert by key
    def add_after_node(self, key, data):
        temp = self.head
        while temp:
            if temp.next is None and temp.data == key:
                self.append_data(data)
                break
            elif temp.data == key:
                node = Node(data, None, None)
                nxt = temp.next
                temp.next = node
                node.next = nxt
                node.prev = temp
                nxt.prev = node
            temp = temp.next

    # Insert before node
    def add_before_node(self, key, data):
        temp = self.head
        while temp:
            if temp.prev is None and temp.data == key:
                self.insert_begin(data)
                return
            elif temp.data == key:
                node = Node(data, None, None)
                prev = temp.prev
                temp.prev = node
                node.prev = prev
                node.next = temp
                prev.next = node
            temp = temp.next

    # Count nodes
    def count_nodes(self):
        count = 0
        if self.head is None:
            raise Exception("Invalid Operation, no data available")
        temp = self.head
        while temp:
            temp = temp.next
            count += 1
        return count

    # Delete node by data
    def delete_by_data(self, key):
        temp = self.head
        while temp:
            if temp.data == key and temp == self.head:
                # Cuando la cabeza solo tiene un nodo
                if not temp.next:
                    self.head = None
                    temp = None
                    return
                else:
                    # Cuando hay varios nodos pero se quiere eliminar el primero
                    nxt = temp.next
                    nxt.prev = None
                    temp = None
                    self.head = nxt
                    return
            elif temp.data == key:
                # Cuando se quiere eliminar el nodo en la mitad
                if temp.next:
                    nxt = temp.next
                    prev = temp.prev
                    nxt.prev = prev
                    prev.next = nxt
                    temp = None
                    return
                else:
                    prev = temp.prev
                    prev.next = None
                    temp = None
                    return
            temp = temp.next

    # Change positions
    def reverse(self):
        if self.head is None:
            raise Exception("No data on the list")
        else:
            container = None
            temp = self.head
            while temp:
                container = temp.prev
                temp.prev = temp.next
                temp.next = container
                temp = temp.prev
            if container:
                self.head = container.prev


