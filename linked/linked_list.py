# Here we have the linked list class with all respective methods

from linked.node import Node


class LinkedList:
    count = 1

    def __init__(self):
        self.head = None

    # Insert data
    def insert_data(self, data):
        if self.head is None:
            node = Node(data, None)
            self.head = node
        elif self.head.data == data:
            print("Data already on the list")
        else:
            node = Node(data, None)
            temp = self.head
            while temp.next:
                temp = temp.next
            temp.next = node

    # Insert to begin
    def insert_begin(self, data):
        if self.head is None:
            node = Node(data, None)
            self.head = node
        elif self.head.data == data:
            raise Exception("Data it's already on the list")
        else:
            temp = Node(data, None)
            temp.next = self.head
            self.head = temp

    # Insert to end
    def insert_end(self, data):
        if self.head is None:
            self.head = Node(data, None)
        else:
            temp = self.head
            while temp.next:
                temp = temp.next
            temp.next = Node(data, None)

    # Insert Multiple items
    def insert_elements(self, data_collection):
        self.head = None
        for item in data_collection:
            self.insert_end(item)

    # Insert by index and data
    def insert_by_data_index(self, index, data):
        if index < 0 or index >= self.count_nodes():
            raise IndexError("Invalid Position")
        elif index == 0:
            self.insert_begin(data)
        else:
            self.count = 0
            temp = self.head
            while temp:
                if self.count == index - 1:
                    node = Node(data, temp.next)
                    temp.next = node
                    break
                temp = temp.next
                self.count += 1

    # Delete Nodes by data
    def delete_nodes_by_data(self, data):
        if self.head is None:
            raise Exception("No data available")
        elif self.head.data == data:
            self.head = self.head.next
        else:
            temp = self.head
            while temp.next:
                if temp.next.data == data:
                    temp.next = temp.next.next
                    break
                temp = temp.next

    # Delete Nodes by position
    def delete_nodes_by_index(self, index):
        if index < 0 or index >= self.count_nodes():
            raise IndexError('Invalid Position')
        elif index == 0:
            self.head = self.head.next
            return
        else:
            self.count = 0
            temp = self.head
            while temp:
                if self.count == index - 1:
                    temp.next = temp.next.next
                    break
                temp = temp.next
                self.count += 1

    # Count Nodes
    def count_nodes(self):
        if self.head is None:
            raise Exception('No data available')
        else:
            cont = 1
            temp = self.head
            while temp.next:
                cont += 1
                temp = temp.next
            return cont

    # Display Data
    def display_list(self):
        if self.head is None:
            raise Exception("No data available")
        else:
            temp = self.head
            temp_str = ''
            while temp:
                temp_str += str(temp.data) + " ---> "
                temp = temp.next
            print(temp_str)

    # Verify if data exist
    def data_exist(self, data):
        if self.head is None:
            raise Exception('No data available')
        elif self.head.data == data:
            return True
        else:
            temp = self.head
            while temp:
                temp = temp.next
                if temp.next.data == data:
                    return True
                raise Exception("Data doesn't exist")

    # Change positions
    def change_position(self, key, key_two):
        if self.head is None:
            raise Exception("No data available")
        else:
            container = None
            temp = self.head
            while temp and temp.data == key:
                container = temp
                temp = temp.next
            container_two = None
            temp_two = self.head
            while temp_two and temp_two.data == key_two:
                container_two = temp_two
                temp_two = temp_two.next
            if not temp.data or not temp_two.data:
                raise Exception("Operation unavailable")

            if container is not None:
                container = temp_two
            else:
                self.head = container_two

            if container_two is not None:
                container_two = temp
            else:
                self.head = container_two







