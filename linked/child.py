"""Class child for the test proposes"""

from linked.location import Location


class Child:
    def __init__(self, name: str, identification: str, age: int, gender: str, location: Location):
        self.__name = name
        self.__identification = identification
        self.__age = age
        self.__gender = gender
        self.__location = location

    # Getter and setters
    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        if len(value) > 15:
            raise Exception("Name to long")
        self.__name = value

    @property
    def identification(self):
        return self.__identification

    @identification.setter
    def identification(self, value):
        self.__identification = value

    @property
    def age(self):
        return self.__identification

    @age.setter
    def age(self, value):
        if value < 0:
            raise Exception("The value need to be greater than zero")
        self.__age = value

    @property
    def gender(self):
        return self.__gender

    @gender.setter
    def gender(self, value):
        self.__gender = value

    @property
    def location(self):
        return self.__location

    @location.setter
    def location(self, value):
        self.__location = value

    def __repr__(self):
        return f'Child({self.__name}, {self.__identification}, {self.__age}, {self.__gender})'
