# Here we have the main class for the node

""" A node for a linked list"""

from linked.child import Child


class Node:
    def __init__(self, data: Child, next: None):
        self.data = data
        self.next = next
