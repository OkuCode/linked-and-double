"""Here we execute the application for the
behaviour of linked and double linked list"""

from linked.linked_list import LinkedList
from linked.double import DoubleLinkedList
from linked.child import Child
from linked.location import Location

if __name__ == '__main__':
    # Child Objects
    location = Location("17001", "Manizales")
    child_one = Child("Juan Carlos", "CH-00", 12, "male", location)
    child_two = Child("Juan David", "CH-01", 15, "male", location)
    child_three = Child("Marcela", "CH-02", 13, "female", location)
    child_four = Child("Emilia", "CH-03", 15, "female", location)
    child_five = Child("Maria", "CH-04", 16, "female", location)

    data_collection = [child_one.name, child_two.name, child_three.name, child_four.name]

    #  Linked list instance
    linked_list = LinkedList()

    # Methods linked list
    linked_list.insert_elements(data_collection)
    linked_list.change_position(child_two.name, child_three.name)
    linked_list.display_list()

    # Instance double list
    double_linked = DoubleLinkedList()

    # Methods double linked list
    double_linked.append_data(child_one.name)
    double_linked.append_data(child_two.name)
    double_linked.append_data(child_three.name)
    double_linked.display_data()
    double_linked.reverse()
    double_linked.display_data()
    print(f'Total de niños: {double_linked.count_nodes()}')

